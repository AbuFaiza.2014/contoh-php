<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php
        echo "<h2>Latihan 1</h2>";
        $kalimat1 = "hallo PHP";
        echo "kalimat pertama : ". $kalimat1 . "<br>";
        echo "panjang string : ". strlen($kalimat1) . "<br>";
        echo "jumlah kata : ". str_word_count($kalimat1) . "<br><br>";
    ?>

    <?php
        $kalimat2 = "I'm ready for the challenges";
        echo "kalimat kedua : ". $kalimat2 . "<br>";
        echo "panjang string : ". strlen($kalimat2) . "<br>";
        echo "jumlah kata : ". str_word_count($kalimat2) . "<br><br>";

        echo "<h2>Latihan 2</h2>";
        $string2 = "I love PHP";
        echo "kalimat kedua: " . $string2. "<br>";
        echo "Kalimat Pertama : " . substr($string2,0,1) . "<br>";
        echo "Kalimat Kedua : " . substr($string2,2,4) . "<br>";
        echo "Kalimat Ketiga : " . substr($string2,7,3) . "<br>";

        echo "<h2>Latihan 3</h2>";
        $string3 = "PHP is old but Good!";
        echo "Kalimat ketiga : ". $string3. "<br>";
        echo "Ganti kalimat ketiga : ". str_replace("Good!","awesome",$string3);
    ?>
 
</body>
</html>