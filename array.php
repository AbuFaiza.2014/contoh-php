<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <h1>Berlatih Array</h1>

    <?php
        echo "<h1>Latihan 1</h1>";
        $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
        print_r($kids);
        echo "<br>";
        $adult = ["Hopper", "Nancy", "Joyce", "Jonathan", "Murray"];
        print_r($adult);

        echo "<h1>Latihan 2</h1>";
        echo "Total kids". count($kids);
        echo "<ol>";
        echo "<li>" . $kids[0] ."</li>";
        echo "<li>" . $kids[1] ."</li>";
        echo "<li>" . $kids[2] ."</li>";
        echo "<li>" . $kids[3] ."</li>";
        echo "<li>" . $kids[4] ."</li>";
        echo "<li>" . $kids[5] ."</li>";
        echo "</ol>";

        echo "Total adult". count($adult);
        echo "<ol>";
        echo "<li>" . $adult[0] ."</li>";
        echo "<li>" . $adult[1] ."</li>";
        echo "<li>" . $adult[2] ."</li>";
        echo "<li>" . $adult[3] ."</li>";
        echo "<li>" . $adult[4] ."</li>";
        echo "</ol>";

        echo "<h1>Latihan 3</h1>";
        $Biodata = [
            ["Nama" => "Will Byers", "Age" => 12, "Aliases" => "Dungeon Master", "Status" => "Alive"],
            ["Nama" => "Mike Wheeler", "Age" => 12, "Aliases" => "Dungeon Master", "Status" => "Alive"],
            ["Nama" => "Jim Hopper", "Age" => 43, "Aliases" => "Chief Hopper", "Status" => "Deceased"],
            ["Nama" => "Eleven", "Age" => 12, "Aliases" => "El", "Status" => "Alive"]
        ];

        echo "<pre>";
        print_r($Biodata);
        echo "</pre>";

    ?>